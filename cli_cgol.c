#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 *	Program name: cli_cgol.c
 *	Author: Matt Whitney
 *	Last Update: 06/12/2019
 *	Function: CLI version of conway's game of life, accepts arguments for grid size.
 *	Compilation: make cli_cgol
 */

/* Fills grid with zeroes */
void genGrid(int r, int c, int g[r][c]) {
    int i,j;
    for (i = 0; i < r; i++) {
        for (j = 0; j < c; j++) {
            g[i][j] = 0;
        }
    }
}

/* Prints the grid with border */
void printGrid(int r, int c, int gen, int g[r][c], char cellChar) {
    int i,j;

    printf(" ");
    for(i = 0; i < c*2-1; i++) {
        printf("-");
    }
    printf("\n");

    for (i = 0; i < r; i++) {
        printf("|");
        for (j = 0; j < c-1; j++) {
            if(g[i][j] == 0) {
                printf("  ");
            } else {
                printf("%c ", cellChar);
            }
        }
        if(g[i][c-1] == 0) {
            printf(" ");
        } else {
            printf("%c", cellChar);
        }

        printf("|\n");
    }

    printf(" ");
    for(i = 0; i < c*2-1; i++) {
        printf("-");
    }
    printf("%d\n", gen);
}

/* Gets data from a .seed file */
int setSeed(int r, int c, int g[r][c], FILE * f1) {
    fseek(f1, 0, SEEK_SET);
    int rowCount = 0;
    int columnCount = 0;
    int i = 0;
    char line[250];

    for(rowCount = 0; rowCount < r; rowCount++) {
        fgets(line, 250, f1);
        i = 0;
        columnCount = 0;
        while(line[i] != '\n' && line[i] != EOF) {
            if(line[i] == '0') {
                columnCount++;
            } else if(line[i] == '1') {
                g[rowCount][columnCount] = 1;
                columnCount++;
            }
            i++;
        }
    }
    return(0);
}

/* Calculates the number of live neighbors a cell has */
int neighbors(int r, int c, int rn, int cn, int g[r][c]) {
    int i, j;
    int neighbors = 0;
    for(i = -1; i < 2; i++) {
        for(j = -1; j < 2;j++) {
            /* Ignore cells out of range */
            if((rn+i < r) && (rn+i > -1) && (cn+j < c) && (cn+j > -1)){
                if(g[rn+i][cn+j] == 1 && !(i == 0 && j == 0)) {
                    neighbors++;
                }
            }
        }
    }
    return(neighbors);
}

/* Calculates the next generation of cells */
int nextGen(int r, int c, int g1[r][c], int g2[r][c]) {
    int i, j, n;
    for(i = 0; i < r; i++) {
        for(j = 0; j < c;j++) {
            n = neighbors(r, c, i, j, g1);
            if(n == 3 && g1[i][j] == 0) {
                g2[i][j] = 1;
            }
            if((n > 1 && n < 4) && g1[i][j] == 1) {
                g2[i][j] = 1;
            }
            if((n < 2 || n > 3) && g1[i][j] == 1) {
                g2[i][j] = 0;
            }
            if(n > 8 || n < 0) {
                return(-1);
            }
        }
    }
    return(0);
}

/* Clears stdin buffer */
void clearBuffer() {
	char c = 0;

	fseek(stdin, 0L, SEEK_END);
	ungetc('\n', stdin); /* Write a '\n' to avoid waiting for user input */
	fseek(stdin, 0L, SEEK_SET);

	while(c != '\n') {
		c = getc(stdin);
	}
}

/* Checks if two arrays are the same */
int sameArray(int r, int c, int g1[r][c], int g2[r][c]) {
    if(memcmp(g1, g2, r * c * sizeof(int)) == 0) {
        return(1);
    }
    return(0);
}

/* Checks if a seed is the right size */
int validSeed(int r, int c, FILE *f1) {
    char ch;
    int i;
    int rowCount = 0;
    int columnCount = 0;

    for(ch = getc(f1); ch != EOF; ch = getc(f1)) {
        if(ch == '\n') {
            rowCount++;
        }
    }
    if(rowCount != r) {
        return(0);
    }

    fseek(f1, 0, SEEK_SET);

    for(i = 0; i < rowCount; i++){
        columnCount = 0;
        for(ch = getc(f1); ch != '\n'; ch = getc(f1)) {
            if(ch == '1' || ch == '0') {
                columnCount++;
            } else if(ch != ' ' && ch != '\n') {
                return(0);
            }
        }
        if(columnCount != c) {
            return(0);
        }
    }
    return(1);
}

int main(int argc, char *argv[]) {
    char ans[10];
    FILE *seedFile;
    int turns = 50;
    int turnsCopy = 50;
    int cols = 40;
    int rows = 20;
    int i = 0;
    char cellChar = 'X';

    /* Argument error checking */
    if(argc > 6 || argc < 5) {
        printf("Usage: ./dynCGOL fileName tickNumber rows columns [cellChar]\n");
        exit(1);
    }
    turns = atoi(argv[2]);
    turnsCopy = turns;
    if(turns < 1) {
        fprintf(stderr, "Error: number of ticks can't be less than 1\n");
        exit(2);
    }
    rows = atoi(argv[3]);
    if(rows < 5 || rows > 100) {
        fprintf(stderr, "Error: number of rows must be in range 5-100\n");
        exit(3);
    }
    cols = atoi(argv[4]);
    if(cols < 5 || cols > 100) {
        fprintf(stderr, "Error: number of columns must be in range 5-100\n");
        exit(4);
    }
    if(argc == 6) {
        if(strlen(argv[5]) > 1) {
            printf("Usage: ./dynCGOL fileName tickNumber rows columns [cellChar]\n");
            exit(5);
        }
        cellChar = (char)(argv[5][0]);
    }

    int grid[rows][cols];
    int nextGrid[rows][cols];

    /* Check seed file exists and is the right size */
    if((seedFile = fopen(argv[1], "r")) == NULL) {
        fprintf(stderr, "Error: file \"%s\" not found\n", argv[1]);
        exit(5);
    }
    if( !validSeed(rows, cols, seedFile) ) {
        fprintf(stderr, "Error: seed is not %dx%d or is of invalid form\n", rows, cols);
        exit(6);
    }

    system("clear");
    genGrid(rows, cols, grid);
    genGrid(rows, cols, nextGrid);
    setSeed(rows, cols, grid, seedFile);
    printGrid(rows, cols, i, grid, cellChar);
    printf("Start? [Y/N]: ");
    fgets(ans, 10, stdin);
    clearBuffer();

    while((strcmp(ans, "Y\n") != 0) && (strcmp(ans, "N\n") != 0) && (strcmp(ans, "y\n") != 0) && (strcmp(ans, "n\n") != 0) ) {
        system("clear");
        printGrid(rows, cols, i, grid, cellChar);
        printf("Start? [Y/N]: ");
        fgets(ans, 10, stdin);
        clearBuffer();
    }
    if((strcmp(ans, "n\n") == 0) || (strcmp(ans, "N\n") == 0)) {
        return(0);
    }

    for(i = 1; i < turns+1; i++) {
        nextGen(rows, cols, grid, nextGrid);
        if(sameArray(rows, cols, nextGrid, grid)) {
            return(0);
        }
        memcpy(grid, nextGrid, sizeof(int) * rows * cols);
        genGrid(rows, cols, nextGrid);
        system("sleep 0.1");
        system("clear");
        printGrid(rows, cols, i, grid, cellChar);
        if(i == turns) {
            printf("Continue? [Y/N]: ");
            fgets(ans, 10, stdin);
            clearBuffer();
            system("clear");
            printGrid(rows, cols, i, grid, cellChar);

            while((strcmp(ans, "Y\n") != 0) && (strcmp(ans, "N\n") != 0) && (strcmp(ans, "y\n") != 0) && (strcmp(ans, "n\n") != 0) ) {
                printf("Continue? [Y/N]: ");
                fgets(ans, 10, stdin);
                clearBuffer();
                system("clear");
                printGrid(rows, cols, i, grid, cellChar);
            }

            if(strcmp(ans, "y\n") == 0 || strcmp(ans, "Y\n") == 0) {
                turns += turnsCopy;
            }
        }
    }
    return(0);
}
