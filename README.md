# CLI Conway's Game of Life



## Compiling

Compile with `make cli_cgol`



## Running

`./cli_cgol dd1 seedfile.seed generations rows columns`



## Seed files

Each row in the file represents a row in the game.

Each row must have the same number of characters (excluding spaces).

In the seed file 1 represents a live cell and 0 represents a dead cell.